from direct.gui.DirectButton import DirectButton
from direct.directbase import DirectStart  
from direct.gui.DirectGui import *   
from direct.interval.IntervalGlobal import * 
from direct.showbase import ShowBase
from panda3d.core import *
from Game import AmdLocalizerEnglish as Localizer
import os
import Game.Globals
from Game import LoadResources as Resources
from direct.actor import Actor
# Introduction to the story

class SceneOne:

    def __init__(self):
        self.diamondback = OnscreenImage(image = "Resources/maps/Diamondback.png", parent=aspect2d)
        self.diamondback.hide()
        self.introaudio = Resources.introaudio
        self.intromusic = Resources.intromusic
        initsequence = Parallel(
            Func(self.introaudio.play),
            Func(self.intromusic.play),
            Func(self.diamondback.show))
        initsequence.start()
w = SceneOne()